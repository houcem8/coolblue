# README #

This app is an evaluation test for iOS developer.

### What is this repository for? ###

* This is an Xcode project
* Created with Xcode version 11.4

### How do I get set up? ###

* Open the project via the file Coolblue.xcodeproj using Xcode 11 or higher
* Wait for Swift packet manager to finish loading the dependancies

### Technical aspects ###

1. Architecture
MVVM architecture: It permits a better separation between the view and the data state of the app, it also makes testing easier. I preferred to not use third party libraries for data binding and to use closures instead.
Coordinator: The coordinator pattern is used within the app to separate the navigation flow from the other components, this provides better separation of concerns and give us the ability to construct each module separately. In this project the AppCoordinator represents the starting point of the app and manage the selection of the first coordinator to start.

2. Dependency Injection
I used the Current/Environment approach to manage dependencies and centralize them in one instance 'Current', this was used in the Kickstarter open source project and it's very powerful for mocking and testing.
Example: https://twitter.com/pointfreeco/status/999265422989037573

3. Product Suggestions
I added a feature for suggesting products when the user is typing in the search box, the datasource of the Suggestions words is a string array.
In real world scenario this feature better be implemented using an Api.
