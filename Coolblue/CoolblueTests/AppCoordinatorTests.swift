//
//  AppCoordinatorTests.swift
//  CoolblueTests
//
//  Created by MacBook Pro on 12/11/2020.
//

import XCTest
@testable import Coolblue

class AppCoordinatorTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testWindowIsKey() {
        let window = WindowStub()
        let coordinator = AppCoordinator(window: window)
        coordinator.start()
        XCTAssertTrue(window.makeKeyAndVisibleCalled)
        XCTAssertNotNil(window.rootViewController)
    }

}

private class WindowStub: UIWindow {
    var makeKeyAndVisibleCalled = false
    override func makeKeyAndVisible() {
        makeKeyAndVisibleCalled = true
    }
}
