//
//  EnvironementMock.swift
//  CoolblueTests
//
//  Created by MacBook Pro on 12/11/2020.
//

import Foundation
@testable import Coolblue

extension Environement {
    static let mock = Environement(suggestionsService: .mock, backendService: .mock)
}
