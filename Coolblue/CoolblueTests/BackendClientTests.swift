//
//  BackendClientTests.swift
//  CoolblueTests
//
//  Created by MacBook Pro on 12/11/2020.
//

import XCTest
@testable import Coolblue

extension BackendClient {
    static let mock = BackendClient(baseUrl: "https://bdk0sta2n0.execute-api.eu-west-1.amazonaws.com/ios-assignment")
}

class BackendClientTests: XCTestCase {
    
    override func setUpWithError() throws {
        Current = .mock
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSearchProducts_success() {
        let exp = expectation(description: "elements returned not nil")
        Current.backendService.searchProducts("iphone", 1) { result in
            
            guard case .success(let products) = result else { XCTAssert(false) ; return }
            exp.fulfill()
            XCTAssertNotNil(products)
        }
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error { XCTFail("Failed on timeout with error \(error)") }
        }
    }

    func testSearchImages_no_keyword() {
        let exp = expectation(description: "return error == invalidResponse")
        Current.backendService.searchProducts(nil, 1) { result in
            guard case .failure(let error) = result else { XCTAssert(false) ; return }
            exp.fulfill()
            XCTAssert(error == .invalidResponse)
        }
        waitForExpectations(timeout: 5.0) { (error) in
            if let error = error { XCTFail("Failed on timeout with error \(error)") }
        }
    }
    
}
