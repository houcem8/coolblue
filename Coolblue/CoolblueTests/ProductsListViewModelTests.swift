//
//  ProductsListViewModelTests.swift
//  CoolblueTests
//
//  Created by MacBook Pro on 12/11/2020.
//

import XCTest
@testable import Coolblue

class ProductsListViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDisplaySearchedProducts_equal_to_sections_cells() throws {
        let viewModel = ProductsListViewModel(backendClient: .dev)
        let productsResult: ProductsResult = ProductsResult.mock
        viewModel.displaySearchedProduct(productsResult, keyword: "iphone")
        
        let sectionCells = viewModel.getSections().first?.products
        XCTAssertEqual(productsResult.products, sectionCells)
    }
}

