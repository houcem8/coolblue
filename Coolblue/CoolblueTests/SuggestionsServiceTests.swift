//
//  SuggestionsServiceTests.swift
//  CoolblueTests
//
//  Created by MacBook Pro on 12/11/2020.
//

import XCTest

extension SuggestionsService {
    static let mock = SuggestionsService(suggestions: ["Apple watch", "Apple iPhone", "Apple Macbook"])
}

class SuggestionsServiceTests: XCTestCase {
    var suggestionsService: SuggestionsService = .mock
    
    override func setUpWithError() throws {
        suggestionsService = .mock
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testGetSuggestions_with_no_keyword() {
        let suggestions = suggestionsService.getSuggestions(forKeyword: nil)
        
        XCTAssertEqual(suggestions, [])
    }
    
    func testGetSuggestions_with_keyword_in_seggestions() {
        let suggestions = suggestionsService.getSuggestions(forKeyword: "apple")
        
        XCTAssertEqual(suggestions.count, 3)
    }
    
    func testGetSuggestions_with_keyword_not_in_seggestions() {
        let suggestions = suggestionsService.getSuggestions(forKeyword: "samsung")
        
        XCTAssertEqual(suggestions.count, 0)
    }

}
