//
//  NetworkError.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import Foundation

enum NetworkError: Error {
    case unknown
    case invalidResponse
    case noInternetConnection
    case noResultFound

    var localizedDescription: String {
        
        switch self {
        case .unknown:
            return "unknown_error"
        case .noResultFound:
            return "no_result_found_error"
        case .invalidResponse:
            return "invalid_response_error"
        case .noInternetConnection:
            return "no_Internet_Connection_error"
        }
    }
}
