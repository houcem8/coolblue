//
//  SuggestionsService.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import Foundation

final class SuggestionsService {
    
    static let prod = SuggestionsService()
    
    var suggestions: [String]
    
    init(suggestions: [String] = []) {
        self.suggestions = (suggestions.count == 0) ? suggestionsDatasource : suggestions
    }
    
    //* Can be fetched from an API in a real world App
    func getSuggestions(forKeyword keyword: String?) -> [String] {
        guard let keyword = keyword else {
            return []
        }
        
        return suggestions.filter {
                    $0.uppercased().contains(keyword.uppercased())
                }.map {
                    $0.capitalized
                }
    }
    
    private let suggestionsDatasource = [
        "Apple iphone X",
        "Apple iphone XS",
        "Apple iphone XR",
        "Apple Macbook Pro",
        "Apple Macbook Air",
        "samsung galaxy S8",
        "samsung galaxy S9",
        "samsung galaxy S10",
        "Samsung TV 55",
        "washing machine",
        "refrigerator",
        "flowers",
        "table",
        "chair",
        "parfum"
    ]
}

