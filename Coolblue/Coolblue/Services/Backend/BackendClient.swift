//
//  BackendClient.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import Foundation

extension BackendClient {
    static let prod = BackendClient(baseUrl: "https://bdk0sta2n0.execute-api.eu-west-1.amazonaws.com/ios-assignment")
    static let dev = BackendClient(baseUrl: "https://bdk0sta2n0.execute-api.eu-west-1.amazonaws.com/ios-assignment")
    
    
}

class BackendClient {
    var baseUrl: String?
    
    private let networkService: NetworkDataFetcher = NetworkDataFetcher()
    
    var searchProducts: (String?,Int, @escaping (Result<ProductsResult, NetworkError>) -> Void) -> Void
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
        self.searchProducts = searchProducts(keyword:page:completion:)
    }
}


func searchProducts(keyword: String?, page: Int = 1, completion: @escaping (Result<ProductsResult, NetworkError>) -> Void) {
    let searchImagesEndPoint = BackendEndPoints(type: .searchProducts(keyword: keyword, page: page))
    
    guard let _ = keyword,
          let url = searchImagesEndPoint.type.value  else {
        completion(.failure(.invalidResponse))
        return
    }
    
    let networkService = NetworkDataFetcher(session: URLSession.shared)
    networkService.performRequest(url: url) { (result) in
        switch result {
        
        case .success(let data):
            do{
                //SUCCESS
                let productsResult = try JSONDecoder().decode(ProductsResult.self, from: data)
                completion(.success(productsResult))
            } catch {
                //ERROR: if json decoder fails
                completion(.failure(.invalidResponse))
            }
        case .failure:
            //ERROR: if request fails
            completion(.failure(.invalidResponse))
            break
        }
    }
}
