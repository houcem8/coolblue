//
//  BackendEndPoints.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import Foundation

struct BackendEndPoints {
    var type: BackendEndPointType
}

enum BackendEndPointType {
    case searchProducts(keyword: String?, page: Int)
    
    var value: URL? {
        switch self {
        case .searchProducts(let keyword, let page):
            guard   let baseUrl = Current.backendService.baseUrl,
                    let keyword = keyword else {
                return nil
            }
            let queryItems = [
                URLQueryItem(name: "query", value: keyword),
                URLQueryItem(name: "page", value: "\(page)")
            ]
            var components = URLComponents(string: baseUrl + "/search")
            components?.queryItems = queryItems
            return components?.url
        }
    }
    
}
