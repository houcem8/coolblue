//
//  SceneDelegate.swift
//  Coolblue
//
//  Created by MacBook Pro on 10/11/2020.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var appCoordinator : AppCoordinator?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let appWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
        appWindow.windowScene = windowScene
        window = appWindow
        
        appCoordinator = AppCoordinator(window: appWindow)
        appCoordinator?.start()
    }
}

