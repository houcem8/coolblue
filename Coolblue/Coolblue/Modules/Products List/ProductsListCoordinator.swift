//
//  ProductsListCoordinator.swift
//  Coolblue
//
//  Created by MacBook Pro on 10/11/2020.
//

import Foundation
import UIKit

class ProductsListCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
        
    var onSearchFinishEvent: (ProductsResult?, String?)->() = { _, _ in }

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }

            guard let viewController = UIStoryboard(name: "Main", bundle: nil)
                .instantiateViewController(withIdentifier: "ProductsListViewController") as? ProductsListViewController else {
                    return
            }
            
            let viewModel = ProductsListViewModel(backendClient: Current.backendService)
            viewController.viewModel = viewModel
            viewController.viewModel?.coordinator = self
            
            self.onSearchFinishEvent = viewModel.displaySearchedProduct
            self.navigationController.setViewControllers([viewController], animated: false)
        }
    }
    
    func showSearchScreen() {
        let searchCoordinator = SearchProductCoordinator(navigationController: navigationController)
        searchCoordinator.parentCoordinator = self
        childCoordinators.append(searchCoordinator)
        searchCoordinator.start()
    }
}
