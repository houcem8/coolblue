//
//  ProductsListViewModel.swift
//  Coolblue
//
//  Created by MacBook Pro on 10/11/2020.
//

import Foundation

class ProductsListViewModel {
    
    var coordinator: ProductsListCoordinator?

    var keyword: String?

    var resetView = {}
    var refreshView = {}
    
    private var backendClient: BackendClient
    private var sections : [Section] = [Section(title: "Search", products: [])]
    private var productsResult : ProductsResult?
    
    init(backendClient: BackendClient) {
        self.backendClient = backendClient
    }
    
    func displaySearchedProduct(_ productsResult: ProductsResult?, keyword: String?) {
        guard let products = productsResult?.products else {
            return
        }
        self.productsResult = productsResult
        self.keyword = keyword
        sections = [Section(title: "Search", products: products)]
        resetView()
        refreshView()
    }
    
    func showSearchScreen() {
        coordinator?.showSearchScreen()
    }
    
    func getSections() -> [Section] {
        sections
    }
}
