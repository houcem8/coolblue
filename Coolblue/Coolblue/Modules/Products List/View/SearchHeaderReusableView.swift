//
//  SearchHeaderReusableView.swift
//  Coolblue
//
//  Created by MacBook Pro on 10/11/2020.
//

import Foundation
import UIKit

class SearchHeaderReusableView: UICollectionReusableView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var searchLabel: UILabel!
    
    lazy var didTapSearchButton: ()->() = {}
    
    static var reuseIdentifier: String {
        return String(describing: SearchHeaderReusableView.self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 10
    }
    
    func setupView(title: String?) {
        searchLabel.text = title ?? "Search for a product ..."
    }
    
    @IBAction func searchAction(_ sender: Any) {
        didTapSearchButton()
    }
    
}
