//
//  ProductsListViewController.swift
//  Coolblue
//
//  Created by MacBook Pro on 10/11/2020.
//

import UIKit

class ProductsListViewController: UICollectionViewController {
    
    var viewModel: ProductsListViewModel?
    
    private lazy var dataSource: DataSource = createDataSource()
    
    // MARK: - Value Types
    typealias DataSource = UICollectionViewDiffableDataSource<Section, Product>
    typealias ProductsSnapshot = NSDiffableDataSourceSnapshot<Section, Product>
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupViewModel()
        applySnapshot(animatingDifferences: false)
    }
    
    func setupView() {
        title = "Search"
        
        collectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
    }
    
    func setupViewModel() {
        viewModel?.resetView = {
            self.collectionView.scrollsToTop = true
            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            self.collectionView.contentOffset = CGPoint(x: 0, y: 0)
        }
        viewModel?.refreshView = {
            self.applySnapshot(animatingDifferences: true)
        }
    }
    
    func createDataSource() -> DataSource {
        let dataSource = DataSource(collectionView: collectionView,cellProvider: {
            (collectionView, indexPath, video) -> UICollectionViewCell? in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell",for: indexPath) as? ProductCollectionViewCell
            
            let section = self.dataSource.snapshot().sectionIdentifiers[indexPath.section]
            cell?.setupCell(product: section.products[indexPath.row])
            return cell
        })
        
        dataSource.supplementaryViewProvider = { [unowned self] collectionView, kind, indexPath in
            guard kind == UICollectionView.elementKindSectionHeader else {
                return nil
            }
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "SearchHeaderReusableView" ,for: indexPath) as? SearchHeaderReusableView
            headerView?.didTapSearchButton = self.didTapSearch
            headerView?.setupView(title: self.viewModel?.keyword)
            return headerView
        }
        
        return dataSource
    }
    
    func applySnapshot(animatingDifferences: Bool = true) {
        let sections = viewModel?.getSections() ?? []
        
        var snapshot = ProductsSnapshot()
        snapshot.appendSections(sections)
        sections.forEach { section in
            snapshot.appendItems(section.products, toSection: section)
        }
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    func didTapSearch() {
        viewModel?.showSearchScreen()
    }
}

extension ProductsListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = UIScreen.main.bounds.width/2
        let cellHeight = cellWidth * 7/5
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
}
