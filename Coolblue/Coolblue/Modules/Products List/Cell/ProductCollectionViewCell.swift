//
//  ProductCollectionViewCell.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.layer.cornerRadius = 6
    }

    func setupCell(product: Product) {
        productTitleLabel.text = product.productName
        productPriceLabel.text = "\(product.salesPriceIncVat)€"
        productImageView.setImage(withUrl: product.productImage)

    }
}
