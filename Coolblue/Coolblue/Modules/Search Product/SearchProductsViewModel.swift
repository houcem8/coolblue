//
//  SearchProductsViewModel.swift
//  Coolblue
//
//  Created by MacBook Pro on 10/11/2020.
//

import Foundation

class SearchProductsViewModel {
    
    private var productSuggestions: [String] = []

    var coordinator: SearchProductCoordinator?
    let suggestionsService: SuggestionsService
    let backendClient: BackendClient
    
    var refreshView: ()->() = {}
    var showLoader: (Bool)-> Void = { _ in }

    init(suggestionsService: SuggestionsService, backendClient: BackendClient) {
        self.suggestionsService = suggestionsService
        self.backendClient = backendClient
    }
    
    func searchProducts(withKeyword keyword: String?) {
        showLoader(true)
        backendClient.searchProducts(keyword, 1) { [weak self] result in
            self?.showLoader(false)
            switch result {
            case .success(let productResult):
                self?.coordinator?.didEndSearch(withProducts: productResult, keyword: keyword)
                break
            case .failure(let error):
                print("error = \(error)")
                break
            }
        }
    }
    
    func refreshProductSuggestions(keyword: String?){
        productSuggestions = suggestionsService.getSuggestions(forKeyword: keyword)
        refreshView()
    }
    
    func getProductSuggestions() -> [String] {
        productSuggestions
    }
    
    func getSuggestion(at index: Int) -> String? {
        guard index >= 0 && index < productSuggestions.count else {
            return nil
        }
        return productSuggestions[index]
    }
    
    func dismissSearchScreen() {
        coordinator?.didEndSearch()
    }
}
