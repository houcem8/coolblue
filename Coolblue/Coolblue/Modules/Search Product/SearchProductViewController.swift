//
//  SearchProductViewController.swift
//  Coolblue
//
//  Created by MacBook Pro on 10/11/2020.
//

import UIKit

class SearchProductViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewModel: SearchProductsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel?.refreshView = tableView.reloadData
        viewModel?.showLoader = { shouldShow in
            shouldShow ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        }
        
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.tintColor = .gray
        searchBar.enablesReturnKeyAutomatically = true
        searchBar.delegate = self
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        searchBar.searchTextField.becomeFirstResponder()
    }
}

extension SearchProductViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.getProductSuggestions().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let suggestion = viewModel?.getSuggestion(at: indexPath.row)
        cell.textLabel?.text = suggestion
        return cell
    }
}

extension SearchProductViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let suggestion = viewModel?.getSuggestion(at: indexPath.row)
        viewModel?.searchProducts(withKeyword: suggestion)
    }
    
}

extension SearchProductViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel?.dismissSearchScreen()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel?.searchProducts(withKeyword: searchBar.text)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel?.refreshProductSuggestions(keyword: searchText)
    }
}
