//
//  SearchProduct.swift
//  Coolblue
//
//  Created by MacBook Pro on 10/11/2020.
//

import Foundation
import UIKit

class SearchProductCoordinator: Coordinator {

    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
        
    var parentCoordinator: Coordinator?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "SearchProductViewController") as? SearchProductViewController else {
                return
        }
        viewController.viewModel = SearchProductsViewModel(suggestionsService: Current.suggestionsService, backendClient: Current.backendService)
        viewController.viewModel?.coordinator = self
        
        viewController.modalPresentationStyle = .currentContext
        
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.present(viewController, animated: false, completion: nil)
    }
    
    func didEndSearch(withProducts productsResult: ProductsResult? = nil, keyword: String? = nil) {
        guard let parentCoordinator = parentCoordinator as? ProductsListCoordinator else {
            return
        }
        parentCoordinator.onSearchFinishEvent(productsResult, keyword)
        parentCoordinator.childDidFinish(childCoordinator: self)
        navigationController.dismiss(animated: false, completion: nil)
    }
    
}
