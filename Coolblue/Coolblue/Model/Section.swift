//
//  Section.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import Foundation

class Section: Hashable {
    var id = UUID()
    var title: String
    var products: [Product]
    
    init(title: String, products: [Product]) {
        self.title = title
        self.products = products
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: Section, rhs: Section) -> Bool {
        lhs.id == rhs.id
    }
}
