//
//  Product.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import Foundation

// MARK: - ProductsResult
struct ProductsResult: Codable {
    var products: [Product]
    let currentPage, pageSize, totalResults, pageCount: Int
    
    init(products: [Product],currentPage: Int ,pageSize: Int ,totalResults: Int, pageCount: Int) {
        self.products = products
        self.currentPage = currentPage
        self.pageSize = pageSize
        self.totalResults = totalResults
        self.pageCount = pageCount
    }
    
    static var mock: ProductsResult = ProductsResult(products: Product.productsMock, currentPage: 1, pageSize: 7, totalResults: 10, pageCount: 1)
}

// MARK: - Product
struct Product: Codable {
    let productId: Int
    let productName: String
    let reviewInformation: ReviewInformation?
    let USPs: [String]
    let availabilityState: Int
    let salesPriceIncVat: Double
    let productImage: String
    let coolbluesChoiceInformationTitle: String?
    let promoIcon: PromoIcon?
    var nextDayDelivery: Bool
    let listPriceIncVat: Int?
    let listPriceExVat: Double?
    
    init(productId: Int,
         productName: String,
         reviewInformation: ReviewInformation? = nil,
         USPs:[String] = [],
         availabilityState: Int = 0,
         salesPriceIncVat: Double = 10,
         productImage: String = "https://....",
         nextDayDelivery: Bool = false)  {
        
        self.productId = productId
        self.productName = productName
        self.reviewInformation = reviewInformation
        self.USPs = USPs
        self.availabilityState = availabilityState
        self.salesPriceIncVat = salesPriceIncVat
        self.productImage = productImage
        self.nextDayDelivery = nextDayDelivery
        
        
        self.coolbluesChoiceInformationTitle = nil
        self.promoIcon = nil
        self.nextDayDelivery = false
        self.listPriceIncVat = nil
        self.listPriceExVat = nil
    }

    static var productsMock: [Product] = [
        Product(productId: 11, productName: "Iphone"),
        Product(productId: 12, productName: "Macbook"),
        Product(productId: 13, productName: "Apple watch"),
    ]
}

extension Product: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(productId)
    }
    
    static func == (lhs: Product, rhs: Product) -> Bool {
        lhs.productId == rhs.productId
    }
}
