//
//  PromoIcon.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import Foundation

// MARK: - PromoIcon
struct PromoIcon: Codable {
    let text: String
    let type: PromoIconType
}

enum PromoIconType: String, Codable {
    case actionPrice = "action-price"
    case coolbluesChoice = "coolblues-choice"
}
