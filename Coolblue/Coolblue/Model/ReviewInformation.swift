//
//  ReviewInformation.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import Foundation

// MARK: - ReviewInformation
struct ReviewInformation: Codable {
    let reviews: [String]
    let reviewSummary: ReviewSummary
}

// MARK: - ReviewSummary
struct ReviewSummary: Codable {
    let reviewAverage: Double
    let reviewCount: Int
}
