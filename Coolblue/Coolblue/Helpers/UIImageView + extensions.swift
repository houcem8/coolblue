//
//  Helpers.swift
//  Coolblue
//
//  Created by MacBook Pro on 12/11/2020.
//

import Foundation
import Nuke
import UIKit

extension UIImageView {
    
    public func setImage(withUrl url: String?) {
        guard let url = url, let imageURL = URL(string: url) else {
            image = UIImage(named: "placeholderImage")
            return
        }
        let options = ImageLoadingOptions(placeholder: UIImage(named: "placeholderImage"),transition: .fadeIn(duration: 0.25))
        Nuke.loadImage(with: imageURL, options: options, into: self)
    }
}

