//
//  AppCoordinator.swift
//  Coolblue
//
//  Created by MacBook Pro on 10/11/2020.
//

import Foundation
import UIKit

protocol Coordinator : class {
    var childCoordinators : [Coordinator] { get set }
    
    func start()
    func childDidFinish(childCoordinator: Coordinator)
}

class AppCoordinator : Coordinator {
    var childCoordinators : [Coordinator] = []
    private var navigationController : UINavigationController!
    private let window : UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        navigationController = UINavigationController()
        
        navigationController.navigationBar.isHidden = false
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.navigationBar.backgroundColor = .white
        navigationController.view.backgroundColor  = .white
        
        let productsListCoordinator = ProductsListCoordinator(navigationController: navigationController)
        childCoordinators.append(productsListCoordinator)
        productsListCoordinator.start()
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}

extension Coordinator {
    func childDidFinish(childCoordinator: Coordinator) {
        if let index = childCoordinators.firstIndex(where: {
            (coordinator: Coordinator) -> Bool in
            childCoordinator === coordinator
        }) {
            childCoordinators.remove(at: index)
        }
    }
}
