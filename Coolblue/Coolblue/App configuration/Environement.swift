//
//  Environement.swift
//  Coolblue
//
//  Created by MacBook Pro on 11/11/2020.
//

import Foundation

//* wrap all project dependencies in the Environement struct
struct Environement {
    private(set) var suggestionsService: SuggestionsService = .prod
    private(set) var backendService: BackendClient = .prod
}

var Current = Environement()
